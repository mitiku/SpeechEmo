import tkinter as tk
import tkinter.messagebox as mb

from tkinter import * 

from speechIO import *

from scipy.io import wavfile

import numpy as np 
import sys
import time
from time import gmtime, strftime

import os
import glob
import numpy

import aifc
import math
import numpy as np 
from numpy import NaN, Inf, arange, isscalar, array
from scipy.fftpack import rfft
from scipy.fftpack import fft
from scipy.fftpack.realtransforms import dct
from scipy.signal import fftconvolve
from matplotlib.mlab import find
import matplotlib.pyplot as plt
from scipy import linalg as la
from scipy.signal import lfilter, hamming

import librosa
import librosa.display
import tensorflow as tf
from matplotlib.pyplot import specgram 


root = tk.Tk()
root.title("Emotion recognition")
root.geometry("1400x650+0+0")
  
def ready():
	mb.showinfo("Demo 1", "please speak a word into the microphone")
	record_to_file('demo.wav')
	mb.showinfo("Demo 1", "Speech recorded")
	sound_input = "demo.wav"
	sample_rate, X = wavfile.read(sound_input)
	#tr_features, tr_labels = parse_audio_files(sound_input)
	features, labels = np.empty((0,180)), np.empty(0)

	X, sample_rate = librosa.load(sound_input)
	stft = np.abs(librosa.stft(X))

	mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T,axis=0)
	chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T,axis=0)

	mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T,axis=0)

	ext_features = np.hstack([mfccs,chroma,mel])
	features = np.vstack([features,ext_features])

	demo_features = np.array(features, dtype = np.float32)

	np.savetxt("demo_features.csv", demo_features, delimiter=',')  # save array value to the text file
	mb.showinfo("Demo 1", "Wait - Checking the model")

	emo_model(demo_features)


def emo_model(demo_features):
    sess = tf.Session()
    new_saver = tf.train.import_meta_graph('my-model-1000.meta')
    new_saver.restore(sess, tf.train.latest_checkpoint('./'))

    graph= tf.get_default_graph()
    w1= graph.get_tensor_by_name("w1:0")
    w2= graph.get_tensor_by_name("w2:0")
    X = graph.get_tensor_by_name("X:0")
    feed_dict={X:demo_features}
    #now, access the op that you want to run.
    op_to_restore= graph.get_tensor_by_name("output:0")
    result = sess.run(op_to_restore,feed_dict)
    print (result)

    var = result.reshape(2,4)
    print ("----------")

    max_value  = 0
    for i in range(2):
    	for j in range(4):
        	if(var[i][j]>max_value):
        		max_value = var[i][j]

	        if(i==0)and (j==0):
	            print ("angry", var[i][j])
	        elif (i==0)and (j==1):
	            print ("exc", var[i][j])
	        elif (i==0)and (j==2):
	            print ("fea", var[i][j])
	        elif (i==0)and (j==3):
	            print ("fru", var[i][j])
	        elif (i==1)and (j==0):
	            print ("hap", var[i][j])
	        elif (i==1)and (j==1):
	            print ("neu", var[i][j])
	        elif (i==1)and (j==2):
	            print ("sad", var[i][j])
	        elif (i==1)and (j==3):
	            print ("sur", var[i][j])

    print ("----------------")            
    for i in range(2):
    	for j in range(4):
	        if(i==0)and (j==0) and (max_value==var[i][j]):
	        	a = "Angry emotion with a value of:"
	        	b = str(var[i][j])
	        	message = a + " " + b
	        	mb.showinfo("Result", message)
	        elif (i==0)and (j==1) and (max_value==var[i][j]):
	        	a = "Exicited emotion with a value"
	        	b = str(var[i][j])
	        	c = a + " " + b
	        	mb.showinfo("Result", c)
	        elif (i==0)and (j==2) and (max_value==var[i][j]):
	        	a = "Fear emotion with a value"
	        	b = str(var[i][j])
	        	c = a + " " + b
	        	mb.showinfo("Result", c)
	        elif (i==0)and (j==3) and (max_value==var[i][j]):
	        	a = "Fra emotion with a value"
	        	b = str(var[i][j])
	        	c = a + " " + b
	        	mb.showinfo("Result", c)
	        elif (i==1)and (j==0) and (max_value==var[i][j]):
	        	a = "Happy emotion with a value"
	        	b = str(var[i][j])
	        	c = a + " " + b
	        	mb.showinfo("Result", c)
	        elif (i==1)and (j==1) and (max_value==var[i][j]):
	        	a = "Neutral emotion with a value"
	        	b = str(var[i][j])
	        	c = a + " " + b
	        	mb.showinfo("Result:", c)
	        elif (i==1)and (j==2) and (max_value==var[i][j]):
	        	a = "Sad emotion with a value"
	        	b = str(var[i][j])
	        	c = a + " " + b
	        	mb.showinfo("Result:", c)
	        elif (i==1)and (j==3) and (max_value==var[i][j]):
	        	a = "Surpriase emotion with a value"
	        	b = str(var[i][j])
	        	c = a + " " + b
	        	mb.showinfo("Result",c)

def result():
	emo_result = "happy"
	return emo_result

lbl1 = tk.Label(root, text="Emotion recognition", width=45, bg="royal blue")
lbl1.pack()

######################################----Frame control ---- #################################
frame1 = tk.LabelFrame(root, text="Control", pady=30)
frame1.pack(fill = "both", expand = "yes")

B1 = tk.Button(frame1, text ="Start Speaking", command = ready, pady= 10)
B1.grid(row=0, column=0)
B1.config(width = 30)

image = tk.PhotoImage(file="speaker.gif")
label = tk.Label(frame1, image=image, padx = 150)
label.grid(row=0, column=1)
label.config(width = 360)

image_emo_default = tk.PhotoImage(file="ready.png")
label = tk.Label(frame1, image=image_emo_default, padx = 150)
label.grid(row=0, column=2)
label.config(width = 360)

'''
emo_result = result()

if emo_result == "happy":
	image = tk.PhotoImage(file="happy.gif")
	label = tk.Label(button_frame, image=image, padx=50)
	label.grid(column=1)
else:
	image = tk.PhotoImage(file="sad.gif")
	label = tk.Label(button_frame, image=image, padx=50)
	label.grid(column=1)

'''

#########################################end##################################################

result_frame = tk.LabelFrame(root, text="Summary")
result_frame.pack(fill = "both", expand = "yes")

root.mainloop()
