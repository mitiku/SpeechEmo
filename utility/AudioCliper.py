

from pydub import AudioSegment

with open('Data.txt') as f:

    lines = f.readlines()

    for line in lines:

        line = line.split()

        if line:

            print "Processing " + str(line)

            sound = AudioSegment.from_mp3(line[2])

            sound = sound[int(float(line[0]) * 1000): int(float(line[1]) * 1000)]

            sound.export("Processed/" + line[3] + "-" + line[0] + "-" + line[1] + "-" + line[2], format="wav")
print "Done!"