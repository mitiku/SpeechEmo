

import sys
import time
import os
import glob

def rename(dir, pattern, titlePattern):
    for pathAndDFilename in glob.iglob(os.path.join(dir, pattern)):
    	title, ext = os.path.splitext(os.path.basename(pathAndDFilename))
        os.rename(pathAndDFilename, os.path.join(dir, titlePattern % title + ext))



parent_dir = 'Sound-Data'
tr_sub_dirs = ["angry"]
# tr_sub_dirs = ["angry","exc","fea","fru","hap","neu","sad","sur"]
# angry, excited, Fear, frustration,happy, neutral, sad, surprised


#parse_audio_files(parent_dir,tr_sub_dirs)

rename(r'Sound-Data/sur/', r'*.wav', r'7-%s')
