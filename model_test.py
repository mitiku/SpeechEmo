
import sys
import time
import os
import glob
import numpy
import cPickle
import aifc
import math
import itertools
from sklearn.metrics import precision_recall_fscore_support
from numpy import NaN, Inf, arange, isscalar, array as np 
from scipy.fftpack import rfft
from scipy.fftpack import fft
from scipy.fftpack.realtransforms import dct
from scipy.signal import fftconvolve
from matplotlib.mlab import find
import matplotlib.pyplot as plt
from scipy import linalg as la
from scipy.signal import lfilter, hamming

import librosa
import librosa.display
import tensorflow as tf
from matplotlib.pyplot import specgram 

from sklearn.metrics import confusion_matrix

import pandas as pd 
import numpy as np 

demo_features = pd.read_csv("demo_features.csv", header=None)


sess = tf.Session()
new_saver = tf.train.import_meta_graph('my-model-1000.meta')
new_saver.restore(sess, tf.train.latest_checkpoint('./'))

#print(sess.run('w1:0'))  # model has been restored.above statement will print the saved value of w1.

# now, let's access and create placeholders variables and
# create feed-dict to feed new data

graph= tf.get_default_graph()

w1= graph.get_tensor_by_name("w1:0")
w2= graph.get_tensor_by_name("w2:0")
X = graph.get_tensor_by_name("X:0")

#feed_dict={w1:demo_features,w2:demo_features}
#feed_dict={X:tr_features,Y:tr_labels})
feed_dict={X:demo_features}

#now, access the op that you want to run.
op_to_restore= graph.get_tensor_by_name("output:0")

result = sess.run(op_to_restore,feed_dict)
#this will print 60 which is calculated 
#using new values of w1 and w2 and saved value of b1.  

#[[ 0.18013395  0.1335755   0.16225775  0.08969519  0.1952558   0.06817966   0.06666955  0.10423259]]
#[[ 0.22551124  0.11273235  0.13415562  0.09192736  0.23255895  0.06773293   0.05729171  0.07808995]]


print (result)
var = result.reshape(2,4)

print ("----------")
max_value  = 0
for i in range(2):
    for j in range(4):

        if(var[i][j]>max_value):
            max_value = var[i][j]

        if(i==0)and (j==0):
            print ("angry", var[i][j])
        elif (i==0)and (j==1):
            print ("exc", var[i][j])
        elif (i==0)and (j==2):
            print ("fea", var[i][j])
        elif (i==0)and (j==3):
            print ("fru", var[i][j])
        elif (i==1)and (j==0):
            print ("hap", var[i][j])
        elif (i==1)and (j==1):
            print ("neu", var[i][j])
        elif (i==1)and (j==2):
            print ("sad", var[i][j])
        elif (i==1)and (j==3):
            print ("sur", var[i][j])

print ("----------")

for i in range(2):
    for j in range(4):

        if(i==0)and (j==0) and (max_value==var[i][j]):
            print ("Angry emotion with a value: ", var[i][j])
        elif (i==0)and (j==1) and (max_value==var[i][j]):
            print ("Excited emotion with a value:", var[i][j])
        elif (i==0)and (j==2) and (max_value==var[i][j]):
            print ("Fear emotion with a value:", var[i][j])
        elif (i==0)and (j==3) and (max_value==var[i][j]):
            print ("frustrated emotion with a value:", var[i][j])
        elif (i==1)and (j==0) and (max_value==var[i][j]):
            print ("Happy emotion with a value:", var[i][j])
        elif (i==1)and (j==1) and (max_value==var[i][j]):
            print ("Neutral emotion with a value:", var[i][j])
        elif (i==1)and (j==2) and (max_value==var[i][j]):
            print ("Sad emotion with a value:", var[i][j])
        elif (i==1)and (j==3) and (max_value==var[i][j]):
            print ("Surpriase emotion with a value:", var[i][j])











