import tkinter as tk
import tkinter.messagebox as mb

from tkinter import * 

from speechIO import *

from scipy.io import wavfile

import numpy as np 
import sys
import time
from time import gmtime, strftime

import os
import glob
import numpy

import aifc
import math
import numpy as np 
from numpy import NaN, Inf, arange, isscalar, array
from scipy.fftpack import rfft
from scipy.fftpack import fft
from scipy.fftpack.realtransforms import dct
from scipy.signal import fftconvolve
from matplotlib.mlab import find
import matplotlib.pyplot as plt
from scipy import linalg as la
from scipy.signal import lfilter, hamming

import librosa
import librosa.display
import tensorflow as tf
from matplotlib.pyplot import specgram 
from threading import Thread
from tkinter import ttk
import pyaudio


root = tk.Tk()
root.title("Emotion recognition")
root.geometry("1400x650+0+0")


RATE = 44100 # SAMPLING RATE OF 44.1 KHz
PYAUDIO_FORMAT = pyaudio.paInt16 
CHANNELS = 2 # stereo channels
CHUNKS = 2**14

EMOTIONS=["Anger","Excitement","Fear","Frustration","Happy","Neutral","Sad","Surprise"]

def ready():
	new_thread=Thread(target=start_processing_thread)
	new_thread.start()
def start_processing_thread():
	p=pyaudio.PyAudio()
	stream = p.open(format=FORMAT, channels=CHANNELS, rate=RATE, input=True, frames_per_buffer=CHUNKS)
	stream.start_stream()
	while (stream.is_active()):
		features, labels = np.empty((0,180)), np.empty(0)
		in_data=stream.read(CHUNKS)
		in_data=np.fromstring(in_data,np.int16)
		print("In data shape",in_data.shape)
		stft = np.abs(librosa.stft(in_data))
		value_to_plot=np.mean(stft)
		print("stft shape",stft.shape)

		mfccs = np.mean(librosa.feature.mfcc(y=in_data, sr=RATE, n_mfcc=40).T,axis=0)
		chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=RATE).T,axis=0)

		mel = np.mean(librosa.feature.melspectrogram(in_data, sr=RATE).T,axis=0)

		ext_features = np.hstack([mfccs,chroma,mel])
		features = np.vstack([features,ext_features])

		demo_features = np.array(features, dtype = np.float32)
		emo_model(demo_features)
	stream.stop_stream()
	stream.close()

	p.terminate()
def get_max_emotion(array):
	max_emotion=array[0][0]
	current_emotion = EMOTIONS[0]
	print("array",array)
	print("max emotion",max_emotion)
	for i in range(8):
		if(array[i//4][i%4]>max_emotion):
			max_emotion = array[i//4][i%4]
			current_emotion = EMOTIONS[i]
	return current_emotion

def emo_model(demo_features):
	global current_emotion
	sess = tf.Session()
	new_saver = tf.train.import_meta_graph('my-model-1000.meta')
	new_saver.restore(sess, tf.train.latest_checkpoint('./'))

	graph= tf.get_default_graph()
	w1= graph.get_tensor_by_name("w1:0")
	w2= graph.get_tensor_by_name("w2:0")
	X = graph.get_tensor_by_name("X:0")
	feed_dict={X:demo_features}
	#now, access the op that you want to run.
	op_to_restore= graph.get_tensor_by_name("output:0")
	result = sess.run(op_to_restore,feed_dict)

	var = result.reshape(2,4)


	print ("----------")
	for i  in range(8):
		emotion_pbs[i]["value"]=int(var[i//4][i%4]*100)
	current_emotion = get_max_emotion(var)
	current_emotion_value["text"]=current_emotion

def result():
	emo_result = "happy"
	return emo_result
current_emotion=""
lbl1 = tk.Label(root, text="Emotion recognition", width=45, bg="royal blue")
lbl1.pack()

######################################----Frame control ---- #################################
frame1 = tk.LabelFrame(root, text="Control", pady=30)
frame1.pack(fill = "both", expand = "yes")

B1 = tk.Button(frame1, text ="Start Speaking", command = ready, pady= 10)
B1.grid(row=0, column=0)
B1.config(width = 30)

image = tk.PhotoImage(file="speaker.gif")
label = tk.Label(frame1, image=image, padx = 150)
label.grid(row=0, column=1)
label.config(width = 360)

image_emo_default = tk.PhotoImage(file="ready.png")
label = tk.Label(frame1, image=image_emo_default, padx = 150)
label.grid(row=0, column=2)
label.config(width = 360)



frame2 = tk.LabelFrame(frame1,text="Emotions")
frame2.grid(row=1,column=1)
frame2.config(width=360)


emotion_labels=[]
emotion_pbs = []
current_emotion_label = tk.Label(frame2,text="Current Emotion",pady=5,anchor="e")
current_emotion_label.grid(row=0,column=0)
current_emotion_label.config(width=20)
current_emotion_value = tk.Label(frame2,text="",pady=5,anchor="w")
current_emotion_value.grid(row=0,column=1)
current_emotion_value.config(width=20)

for i in range(len(EMOTIONS)):
	emotion_label = tk.Label(frame2,text=EMOTIONS[i],pady=5,anchor="e")
	emotion_label.grid(row=i+1,column=0)
	emotion_label.config(width=20)
	emotion_labels.append(emotion_label)

	emotion_pb = ttk.Progressbar(frame2,orient=HORIZONTAL,length=200,mode='determinate')
	emotion_pb.grid(row=i+1,column=1)
	emotion_pb["value"]=0
	emotion_pbs.append(emotion_pb)


result_frame = tk.LabelFrame(root, text="Summary")
result_frame.pack(fill = "both", expand = "yes")

root.mainloop()
